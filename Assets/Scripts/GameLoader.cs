using UnityEngine;
using Base;
using Sirenix.OdinInspector;

public class GameLoader : MonoBehaviour
{
    [SerializeField] private int _fps = 120;
    [SerializeField] private Material _skyBox;

    [Header("Class Init")]
    [SerializeField] private CharacterBuilder _characterBuilder;

    [SerializeField] GameWorld _gameWorld;

    private void Start()
    {
        Application.targetFrameRate = _fps;
        SetSkyBox();

        Init();
    }

    private void Init()
    {
        _gameWorld.Init(_characterBuilder.Character);

        _characterBuilder.Init(_gameWorld);
        _characterBuilder.RespawnPlayer();
    }

    [Button("Change SkyBox", ButtonStyle.Box)]
    private void SetSkyBox()
    {
        if (_skyBox == null)
            return;
        RenderSettings.skybox = _skyBox;
    }
}
