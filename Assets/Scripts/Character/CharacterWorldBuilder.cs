using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;

[RequireComponent(typeof(Character))]
public class CharacterWorldBuilder : MonoBehaviour
{
    private const float ACTION_DISTANSE = 10f;

    private Camera _playerCamera;
    private GameWorld _world;

    public void Init(GameWorld world, Camera playerCamera)
    {
        _world = world;
        _playerCamera = playerCamera;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            BuildBlock(BlockType.Wood);

        if (Input.GetMouseButtonDown(1))
            DestroyBlock();
    }

    public void BuildBlock(BlockType block)
    {
        if (TryHitGround(out RaycastHit hitInfo))
        {
            Vector3 blockCenter = hitInfo.point + hitInfo.normal * MeshBuilder.BLOCK_SCALE / 2;
            Vector3Int blockWorldPos = Vector3Int.FloorToInt(blockCenter / MeshBuilder.BLOCK_SCALE);
            _world.BuildBlockInChunk(blockWorldPos, block);
        }
    }

    public void DestroyBlock()
    {
        if (TryHitGround(out RaycastHit hitInfo))
        {
            Vector3 blockCenter = hitInfo.point - hitInfo.normal * MeshBuilder.BLOCK_SCALE / 2;
            Vector3Int blockWorldPos = Vector3Int.FloorToInt(blockCenter / MeshBuilder.BLOCK_SCALE);
            _world.DestroyBlockInChunk(blockWorldPos);
        }
    }

    public bool TryHitGround(out RaycastHit hitInfo)
    {
        hitInfo = new();
        Ray _centerRay = _playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(_centerRay, out hitInfo))
        {
            if (Vector3.Distance(hitInfo.point, transform.position) > ACTION_DISTANSE)
                return false;

            return true;
        }
        return false;
    }
}
