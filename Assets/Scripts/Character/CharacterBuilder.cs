using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBuilder : MonoBehaviour
{
    [field: SerializeField] public Character Character { get; private set; }

    [SerializeField] private Vector3 _characterSpawnPosition = Vector3.zero;
    [SerializeField] private Camera _playerCamera;

    private GameWorld _gameWorld;

    public void Init(GameWorld gameWorld)
    {
        _gameWorld = gameWorld;

        Character.Init();

        var characterWorldBuilder = Character.gameObject.AddComponent<CharacterWorldBuilder>();
        characterWorldBuilder.Init(_gameWorld, _playerCamera);
    }

    public void RespawnPlayer()
    {
        Character.transform.position = _characterSpawnPosition;
        Character.Movement.SetMoveCondition(true);
    }
}
