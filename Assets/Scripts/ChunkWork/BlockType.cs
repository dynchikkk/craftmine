public enum BlockType : byte
{
    Air = 0,
    Wood = 1,
    Grass = 2,
    Stone = 3
}
