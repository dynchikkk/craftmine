using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratedMeshData
{
    public GeneratedMeshVertex[] Vertices;
    public Bounds Bounds;
    public ChunkData ChunkData;
}

[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
public struct GeneratedMeshVertex
{
    public Vector3 pos;
    public sbyte normalX, normalY, normalZ, normalW;
    public ushort uvX, uvY;
}
