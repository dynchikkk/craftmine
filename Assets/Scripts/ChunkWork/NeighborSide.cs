using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NeighborSide
{
    Left = 0, 
    Right = 1, 
    Forward = 2, 
    Back = 3
}
