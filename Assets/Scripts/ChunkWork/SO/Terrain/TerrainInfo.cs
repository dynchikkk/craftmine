using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TerrainConfig", menuName = "Configs/Terrain Config")]
public class TerrainInfo : ScriptableObject
{
    public float BaseHeight = 8f;
    public NoiseOctaveSettings DomainWarp;
    [Space(10)]
    public NoiseOctaveSettings[] OctaveSettings;
}


[Serializable]
public class NoiseOctaveSettings
{
    public FastNoiseLite.NoiseType NoiseType;
    public float Frequency = 0.2f;
    public float Amplitude = 1f;
}
