using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultBlockConfig", menuName = "Configs/Blocks/Default Block Config")]
public class DefaultBlockInfo : BlockInfo
{
    public override (ushort, ushort) GetPixelsOffset(Vector3 normal)
    {
        return (_x, _y);
    }
}
