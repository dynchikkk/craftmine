using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BlockInfo : ScriptableObject
{
    [Header("Base")]
    public BlockType Type;
    public float BlockHealth = 10f;

    [Header("Texture")]
    [SerializeField] protected ushort _x;
    [SerializeField] protected ushort _y;

    [Header("Sound")]
    public AudioClip StepSound;

    public abstract (ushort, ushort) GetPixelsOffset(Vector3 normal);
}
