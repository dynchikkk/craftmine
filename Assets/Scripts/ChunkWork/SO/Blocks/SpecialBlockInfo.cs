using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpecialBlockConfig", menuName = "Configs/Blocks/Special Block Config")]
public class SpecialBlockInfo : BlockInfo
{
    [Header("Textures dop up")]
    [SerializeField] private ushort _xUp;
    [SerializeField] private ushort _yUp;
    [Header("Textures dop down")]
    [SerializeField] private ushort _xDown;
    [SerializeField] private ushort _yDown;

    public override (ushort, ushort) GetPixelsOffset(Vector3 normal)
    {
        if (normal == Vector3.up) return (_xUp, _yUp);
        if (normal == Vector3.down) return (_xDown, _yDown);

        return (_x, _y);
    }
}
