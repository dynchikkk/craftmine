using Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockHolder : Singleton<BlockHolder>
{
    [SerializeField] private BlockInfo[] _blockDatas;

    private Dictionary<BlockType, BlockInfo> _blockInfos = new();

    protected override void Awake()
    {
        base.Awake();

        SetBlockDictianory();

        DontDestroyOnLoad(gameObject);
    }

    private void SetBlockDictianory()
    {
        foreach (BlockInfo block in _blockDatas)
            _blockInfos.Add(block.Type, block);
    }

    public BlockInfo GetBlockByType(BlockType blockType)
    {
        if (_blockInfos.TryGetValue(blockType, out BlockInfo data))
            return data;
        return null;
    }

    public bool TryGetBlockByType(BlockType blockType, out BlockInfo data)
    {
        data = GetBlockByType(blockType);
        if (data != null)
            return true;
        return false;
    }
}
