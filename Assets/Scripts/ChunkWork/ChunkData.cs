using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ChunkData
{
    private const int CHUNK_WIDTH_SQ = MeshBuilder.CHUNK_WIDTH * MeshBuilder.CHUNK_WIDTH;

    public ChunkDataState State { get; private set; }

    public Vector2Int ChunkPosition;
    public ChunkRenderer ChunkRenderer;
    private BlockType[] _blocks;

    public Dictionary<Vector2Int, ChunkData> NeighborsChunk { get; private set; } = new Dictionary<Vector2Int, ChunkData>()
    {
        {Vector2Int.left, null },
        {Vector2Int.right, null },
        {Vector2Int.up, null },
        {Vector2Int.down, null }
    };

    public void SetState(ChunkDataState state)
        => State = state;

    public void SetChunkNeighbors(GameWorld gameWorld)
    {
        SetNeighbor(Vector2Int.left, gameWorld);
        SetNeighbor(Vector2Int.right, gameWorld);
        SetNeighbor(Vector2Int.up, gameWorld);
        SetNeighbor(Vector2Int.down, gameWorld);
    }

    private void SetNeighbor(Vector2Int side, GameWorld gameWorld)
    {
        if (NeighborsChunk[side] != null)
            return;

        var datas = gameWorld.ChunkDatas;
        var chunkPos = ChunkPosition;

        datas.TryGetValue(chunkPos + side, out ChunkData neighbor);
        NeighborsChunk[side] = neighbor;
    }

    public bool TryGetNeighbor(Vector2Int side, out ChunkData neighbor)
    {
        neighbor = null;
        if (NeighborsChunk[side] == null)
            return false;

        neighbor = NeighborsChunk[side];
        return true;
    }

    public bool TryGetBlock(Vector3Int blockPos, out BlockType block)
    {
        block = BlockType.Air;
        int index = CalculateArrayIndex(blockPos);

        if (index >= _blocks.Length)
            return false;

        block = _blocks[index];

        return true;
    }

    public bool TrySetBlock(Vector3Int blockPos, BlockType blockType)
    {
        int index = CalculateArrayIndex(blockPos);

        if (index >= _blocks.Length)
            return false;

        _blocks[index] = blockType;

        return true;
    }

    public void SetBlocks(BlockType[] blocks) =>
        _blocks = blocks;

    private int CalculateArrayIndex(Vector3Int blockPos)
    {
        int index = blockPos.x + blockPos.y * CHUNK_WIDTH_SQ + blockPos.z * MeshBuilder.CHUNK_WIDTH;
        return index;
    }
}

public enum ChunkDataState
{
    StartedLoading = 0,
    Loaded = 1,
    StartedMeshing = 2,
    SpawnedOnScene = 3,
    Unloaded = 4
}
