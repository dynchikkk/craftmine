using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Profiling;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public class ChunkRenderer : MonoBehaviour
{
    private ChunkData _chunkData;
    private GameWorld _gameWorld;

    private static int[] _triangles;

    private Mesh _chunkMesh;
    private MeshFilter _meshFilter;
    private MeshCollider _meshCollider;

    private static ProfilerMarker RenderingMarker = new ProfilerMarker(ProfilerCategory.Loading, "Rendering");

    public void Init(ChunkData chunkData, GameWorld gameWorld)
    {
        _chunkData = chunkData;
        _gameWorld = gameWorld;

        _chunkMesh = new Mesh();
        _meshFilter = GetComponent<MeshFilter>();
        _meshCollider = GetComponent<MeshCollider>();
        _meshFilter.mesh = _chunkMesh;
    }

    public static void InitTriangles()
    {
        // all triangles
        _triangles = new int[65536 * 6 / 4];

        int vertexNum = 4;
        for (int i = 0; i < _triangles.Length; i += 6)
        {
            _triangles[i] = vertexNum - 4;
            _triangles[i + 1] = vertexNum - 3;
            _triangles[i + 2] = vertexNum - 2;

            _triangles[i + 3] = vertexNum - 3;
            _triangles[i + 4] = vertexNum - 1;
            _triangles[i + 5] = vertexNum - 2;

            vertexNum += 4;
        }
    }

    public void SetMesh(GeneratedMeshData meshData)
    {
        RenderingMarker.Begin();

        var layout = new[]
        {
            new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
            new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.SNorm8, 4),
            new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.UNorm16, 2),
        };
        var vertexCount = meshData.Vertices.Length;
        _chunkMesh.SetVertexBufferParams(vertexCount, layout);
        _chunkMesh.SetVertexBufferData(meshData.Vertices, 0, 0, vertexCount);

        int trianglesCount = meshData.Vertices.Length * 6 / 4;
        _chunkMesh.SetIndexBufferParams(trianglesCount, IndexFormat.UInt32);
        _chunkMesh.SetIndexBufferData(_triangles, 0, 0, trianglesCount);

        _chunkMesh.subMeshCount = 1;
        _chunkMesh.SetSubMesh(0, new SubMeshDescriptor(0, trianglesCount));

        _chunkMesh.bounds = meshData.Bounds;
        _meshCollider.sharedMesh = _chunkMesh;

        RenderingMarker.End();
    }

    
    #region Build && Destroy
    public void BuildBlock(Vector3Int blockPos, BlockType blockType)
    {
        if (!_chunkData.TrySetBlock(blockPos, blockType))
            return;

        RegenerateMesh();
    }

    public void DestroyBlock(Vector3Int blockPosition)
    {
        if (!_chunkData.TrySetBlock(blockPosition, BlockType.Air))
            return;

        RegenerateMesh();

        if (blockPosition.x <= 0)
        {
            if (_chunkData.TryGetNeighbor(Vector2Int.left, out ChunkData data))
                data.ChunkRenderer.RegenerateMesh();
        }
        if (blockPosition.x >= MeshBuilder.CHUNK_WIDTH - 1)
        {
            if (_chunkData.TryGetNeighbor(Vector2Int.right, out ChunkData data))
                data.ChunkRenderer.RegenerateMesh();
        }
        if (blockPosition.z <= 0)
        {
            if (_chunkData.TryGetNeighbor(Vector2Int.down, out ChunkData data))
                data.ChunkRenderer.RegenerateMesh();
        }
        if (blockPosition.z >= MeshBuilder.CHUNK_WIDTH - 1)
        {
            if (_chunkData.TryGetNeighbor(Vector2Int.up, out ChunkData data))
                data.ChunkRenderer.RegenerateMesh();
        }
    }

    public void RegenerateMesh()
    {
        SetMesh(MeshBuilder.GenerateMesh(_chunkData));
    }
    #endregion 
}
