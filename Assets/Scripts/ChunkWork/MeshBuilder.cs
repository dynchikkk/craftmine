using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshBuilder
{
    public const int CHUNK_WIDTH = 32;
    public const int CHUNK_HEIGHT = 128;
    public const float BLOCK_SCALE = 0.125f;

    private const ushort BROKEN_TEXTURE_X = 160;
    private const ushort BROKEN_TEXTURE_Y = 224;

    public static GeneratedMeshData GenerateMesh(ChunkData chunkData)
    {
        List<GeneratedMeshVertex> vertices = new List<GeneratedMeshVertex>();

        int maxY = 0;
        for (int y = 0; y < CHUNK_HEIGHT; y++)
        {
            for (int x = 0; x < CHUNK_WIDTH; x++)
            {
                for (int z = 0; z < CHUNK_WIDTH; z++)
                {
                    if (GenerateBlock(x, y, z, vertices, chunkData))
                    {
                        if (y > maxY)
                            maxY = y;
                    }
                }
            }
        }

        var mesh = new GeneratedMeshData();
        mesh.Vertices = vertices.ToArray();

        Vector3 boundsSize = new Vector3(CHUNK_WIDTH, maxY, CHUNK_WIDTH) * BLOCK_SCALE;
        mesh.Bounds = new Bounds(boundsSize / 2, boundsSize);

        mesh.ChunkData = chunkData;

        return mesh;
    }

    private static bool GenerateBlock(int x, int y, int z, List<GeneratedMeshVertex> vertices, ChunkData chunkData)
    {
        var blockPosition = new Vector3Int(x, y, z);

        if (GetBlockAtPosition(blockPosition, chunkData) == BlockType.Air)
            return false;

        BlockType blockType = GetBlockAtPosition(blockPosition, chunkData);

        if (GetBlockAtPosition(blockPosition + Vector3Int.right, chunkData) == BlockType.Air)
            GenerateRiteSide(blockPosition, vertices, blockType);

        if (GetBlockAtPosition(blockPosition + Vector3Int.left, chunkData) == BlockType.Air)
            GenerateLeftSide(blockPosition, vertices, blockType);

        if (GetBlockAtPosition(blockPosition + Vector3Int.forward, chunkData) == BlockType.Air)
            GenerateFrontSide(blockPosition, vertices, blockType);

        if (GetBlockAtPosition(blockPosition + Vector3Int.back, chunkData) == BlockType.Air)
            GenerateBackSide(blockPosition, vertices, blockType);

        if (GetBlockAtPosition(blockPosition + Vector3Int.up, chunkData) == BlockType.Air)
            GenerateUpSide(blockPosition, vertices, blockType);

        if (blockPosition.y > 0 && GetBlockAtPosition(blockPosition + Vector3Int.down, chunkData) == BlockType.Air)
            GenerateDownSide(blockPosition, vertices, blockType);

        return true;
    }

    private static BlockType GetBlockAtPosition(Vector3Int blockPosition, ChunkData chunkData)
    {
        if (blockPosition.x >= 0 && blockPosition.x < CHUNK_WIDTH &&
            blockPosition.y >= 0 && blockPosition.y < CHUNK_HEIGHT &&
            blockPosition.z >= 0 && blockPosition.z < CHUNK_WIDTH)
        {
            chunkData.TryGetBlock(blockPosition, out BlockType block);
            return block;
        }

        if (blockPosition.y < 0 || blockPosition.y >= CHUNK_HEIGHT)
            return BlockType.Air;

        BlockType GetBlock(Vector2Int side, Vector3Int blockPos)
        {
            if (chunkData.TryGetNeighbor(side, out ChunkData neighbor))
            {
                neighbor.TryGetBlock(blockPos, out BlockType block);
                return block;
            }
            return BlockType.Air;
        }

        if (blockPosition.x < 0)
        {
            blockPosition.x += CHUNK_WIDTH;
            return GetBlock(Vector2Int.left, blockPosition);
        }

        if (blockPosition.x >= CHUNK_WIDTH)
        {
            blockPosition.x -= CHUNK_WIDTH;
            return GetBlock(Vector2Int.right, blockPosition);
        }

        if (blockPosition.z < 0)
        {
            blockPosition.z += CHUNK_WIDTH;
            return GetBlock(Vector2Int.down, blockPosition);
        }

        if (blockPosition.z >= CHUNK_WIDTH)
        {
            blockPosition.z -= CHUNK_WIDTH;
            return GetBlock(Vector2Int.up, blockPosition);
        }

        return BlockType.Air;
    }

    #region BlockSidesGenerate
    private static bool GenerateRiteSide(Vector3Int blockPos, List<GeneratedMeshVertex> vertices, BlockType block)
    {
        GeneratedMeshVertex vertex = new();
        vertex.normalX = 1;
        vertex.normalY = 0;
        vertex.normalZ = 0;
        vertex.normalW = 1;

        GetUvs(block, Vector3Int.right, out vertex.uvX, out vertex.uvY);

        vertex.pos = BLOCK_SCALE * (new Vector3(1, 0, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 1, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 0, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 1, 1) + blockPos);
        vertices.Add(vertex);

        return true;
    }

    private static bool GenerateLeftSide(Vector3Int blockPos, List<GeneratedMeshVertex> vertices, BlockType block)
    {
        GeneratedMeshVertex vertex = new();
        vertex.normalX = sbyte.MinValue;
        vertex.normalY = 0;
        vertex.normalZ = 0;
        vertex.normalW = 1;

        GetUvs(block, Vector3Int.left, out vertex.uvX, out vertex.uvY);

        vertex.pos = BLOCK_SCALE * (new Vector3(0, 0, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 0, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 1, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 1, 1) + blockPos);
        vertices.Add(vertex);

        return true;
    }

    private static bool GenerateFrontSide(Vector3Int blockPos, List<GeneratedMeshVertex> vertices, BlockType block)
    {
        GeneratedMeshVertex vertex = new();
        vertex.normalX = 0;
        vertex.normalY = 0;
        vertex.normalZ = sbyte.MaxValue;
        vertex.normalW = 1;

        GetUvs(block, Vector3Int.forward, out vertex.uvX, out vertex.uvY);

        vertex.pos = BLOCK_SCALE * (new Vector3(0, 0, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 0, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 1, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 1, 1) + blockPos);
        vertices.Add(vertex);

        return true;
    }

    private static bool GenerateBackSide(Vector3Int blockPos, List<GeneratedMeshVertex> vertices, BlockType block)
    {
        GeneratedMeshVertex vertex = new();
        vertex.normalX = 0;
        vertex.normalY = 0;
        vertex.normalZ = sbyte.MinValue;
        vertex.normalW = 1;

        GetUvs(block, Vector3Int.back, out vertex.uvX, out vertex.uvY);

        vertex.pos = BLOCK_SCALE * (new Vector3(0, 0, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 1, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 0, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 1, 0) + blockPos);
        vertices.Add(vertex);

        return true;
    }

    private static bool GenerateUpSide(Vector3Int blockPos, List<GeneratedMeshVertex> vertices, BlockType block)
    {
        GeneratedMeshVertex vertex = new();
        vertex.normalX = 0;
        vertex.normalY = sbyte.MaxValue;
        vertex.normalZ = 0;
        vertex.normalW = 1;

        GetUvs(block, Vector3Int.up, out vertex.uvX, out vertex.uvY);

        vertex.pos = BLOCK_SCALE * (new Vector3(0, 1, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 1, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 1, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 1, 1) + blockPos);
        vertices.Add(vertex);

        return true;
    }

    private static bool GenerateDownSide(Vector3Int blockPos, List<GeneratedMeshVertex> vertices, BlockType block)
    {
        GeneratedMeshVertex vertex = new();
        vertex.normalX = 0;
        vertex.normalY = sbyte.MinValue;
        vertex.normalZ = 0;
        vertex.normalW = 1;

        GetUvs(block, Vector3Int.down, out vertex.uvX, out vertex.uvY);

        vertex.pos = BLOCK_SCALE * (new Vector3(0, 0, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 0, 0) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(0, 0, 1) + blockPos);
        vertices.Add(vertex);
        vertex.pos = BLOCK_SCALE * (new Vector3(1, 0, 1) + blockPos);
        vertices.Add(vertex);

        return true;
    }

    private static void GetUvs(BlockType blockType, Vector3Int normal, out ushort x, out ushort y)
    {
        x = BROKEN_TEXTURE_X;
        y = BROKEN_TEXTURE_Y;

        if (BlockHolder.Instance.TryGetBlockByType(blockType, out BlockInfo blockData))
            (x, y) = blockData.GetPixelsOffset(normal);

        x *= 256;
        y *= 256;
    }
    #endregion
}
