using Base;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using static UnityEngine.Mesh;

[RequireComponent(typeof(TerrainGenerator))]
public class GameWorld : MonoBehaviour
{
    [SerializeField] private int _viewRadius = 5;
    [SerializeField] private ChunkRenderer _chunkPrefab;

    public Dictionary<Vector2Int, ChunkData> ChunkDatas { get; private set; } = new();

    private Vector2Int _currentCharacterChunk = Vector2Int.zero;
    private Character _character;
    private TerrainGenerator _terrainGenerator;
    private Queue<GeneratedMeshData> _generatedMeshes = new();

    public void Init(Character character)
    {
        ChunkRenderer.InitTriangles();

        _character = character;

        _terrainGenerator = GetComponent<TerrainGenerator>();
        _terrainGenerator.Init();

        StartCoroutine(SetUpWorld());
        SubOnEvent();
    }

    private void Update()
    {
        if(_generatedMeshes.TryDequeue(out GeneratedMeshData meshData))
        {
            if (meshData == null)
                Debug.Log("Error");
            SpawnRenderedChunk(meshData);
            meshData.ChunkData.SetState(ChunkDataState.SpawnedOnScene);
        }
    }

    private void OnDisable()
    {
        UnSubOnEvent();
    }

    private void SubOnEvent()
    {
        _character.OnMove += CheckChunkByTransform;
    }

    private void UnSubOnEvent()
    {
        _character.OnMove -= CheckChunkByTransform;
    }

    private IEnumerator SetUpWorld()
    {
        List<ChunkData> chunkQueue = new List<ChunkData>();
        int loadRadius = _viewRadius + 1;
        Vector2Int center = _currentCharacterChunk;

        Vector2Int xRadiusLoad = new Vector2Int(center.x - loadRadius, center.x + loadRadius);
        Vector2Int yRadiusLoad = new Vector2Int(center.y - loadRadius, center.y + loadRadius);
        for (int x = xRadiusLoad.x; x <= xRadiusLoad.y; x++)
        {
            for (int y = yRadiusLoad.x; y <= yRadiusLoad.y; y++)
            {
                Vector2Int chunkPos = new Vector2Int(x, y);
                if (ChunkDatas.TryGetValue(chunkPos, out ChunkData data))
                    continue;

                ChunkData loadingChunk = LoadChunkAt(chunkPos);
                chunkQueue.Add(loadingChunk);
            }
        }

        bool IsAllLoading()
        {
            int count = chunkQueue.Count;
            foreach (ChunkData chunk in chunkQueue)
            {
                if (chunk.State == ChunkDataState.StartedLoading)
                    return false;
            }
            return true;
        }

        yield return new WaitWhile(IsAllLoading);

        Vector2Int xRadiusView = new Vector2Int(center.x - _viewRadius, center.x + _viewRadius);
        Vector2Int yRadiusView = new Vector2Int(center.y - _viewRadius, center.y + _viewRadius);
        for (int x = xRadiusView.x; x <= xRadiusView.y; x++)
        {
            for (int y = yRadiusView.x; y <= yRadiusView.y; y++)
            {
                Vector2Int chunkPos = new Vector2Int(x, y);
                ChunkData data = ChunkDatas[chunkPos];
                if (data.ChunkRenderer != null)
                    continue;

                StartRenderChunk(data);
            }
        }
    }

    private ChunkData LoadChunkAt(Vector2Int pos)
    {
        int x = pos.x;
        int y = pos.y;
        float xPos = x * MeshBuilder.CHUNK_WIDTH * MeshBuilder.BLOCK_SCALE;
        float zPos = y * MeshBuilder.CHUNK_WIDTH * MeshBuilder.BLOCK_SCALE;

        ChunkData chunkData = new();
        chunkData.SetState(ChunkDataState.StartedLoading);
        chunkData.ChunkPosition = new Vector2Int(x, y);

        ChunkDatas.Add(new Vector2Int(x, y), chunkData);

        Task.Factory.StartNew(() =>
        {
            chunkData.SetBlocks(_terrainGenerator.GenerateTerrain(xPos, zPos));
            chunkData.SetState(ChunkDataState.Loaded);
        });

        return chunkData;
    }

    private void StartRenderChunk(ChunkData chunkData)
    {
        chunkData.SetChunkNeighbors(this);

        chunkData.SetState(ChunkDataState.StartedMeshing);

        Task.Factory.StartNew(() =>
        {
            GeneratedMeshData meshData = MeshBuilder.GenerateMesh(chunkData);
            if (meshData == null)
                Debug.Log("Error");
            _generatedMeshes.Enqueue(meshData);
        });
    }

    private void SpawnRenderedChunk(GeneratedMeshData meshData)
    {
        float xPos = meshData.ChunkData.ChunkPosition.x * MeshBuilder.CHUNK_WIDTH * MeshBuilder.BLOCK_SCALE;
        float zPos = meshData.ChunkData.ChunkPosition.y * MeshBuilder.CHUNK_WIDTH * MeshBuilder.BLOCK_SCALE;

        var chunk = Instantiate(_chunkPrefab, new Vector3(xPos, 0, zPos), Quaternion.identity, transform);
        meshData.ChunkData.ChunkRenderer = chunk;
        chunk.Init(meshData.ChunkData, this);

        chunk.SetMesh(meshData);
    }

    private Vector2Int GetChunkByWorldPosition(Vector3Int blockWorldPos)
    {
        Vector2Int chunkPosition = new Vector2Int(blockWorldPos.x / MeshBuilder.CHUNK_WIDTH, blockWorldPos.z / MeshBuilder.CHUNK_WIDTH);
        if (blockWorldPos.x < 0)
            chunkPosition.x--;
        if (blockWorldPos.z < 0)
            chunkPosition.y--;

        return chunkPosition;
    }

    private void CheckChunkByTransform(Vector3 position)
    {
        Vector3Int playerWorldPos = Vector3Int.FloorToInt(position / MeshBuilder.BLOCK_SCALE);
        Vector2Int playerChunk = GetChunkByWorldPosition(playerWorldPos);

        if(_currentCharacterChunk != playerChunk)
        {
            _currentCharacterChunk = playerChunk;
            StartCoroutine(SetUpWorld());
        }
    }

    #region Build && Destroy Block
    public void BuildBlockInChunk(Vector3Int blockWorldPos, BlockType blockType)
    {
        if (TryGetRendererAndLocalBlock(blockWorldPos, out ChunkRenderer renderer, out Vector3Int blockLocalPos))
            renderer.BuildBlock(blockLocalPos, blockType);
    }

    public void DestroyBlockInChunk(Vector3Int blockWorldPos)
    {
        if (TryGetRendererAndLocalBlock(blockWorldPos, out ChunkRenderer renderer, out Vector3Int blockLocalPos))
            renderer.DestroyBlock(blockLocalPos);
    }

    private bool TryGetRendererAndLocalBlock(Vector3Int blockWorldPos, out ChunkRenderer renderer, out Vector3Int blockLocalPos)
    {
        renderer = null;
        blockLocalPos = Vector3Int.zero;

        Vector2Int chunkPosV2 = GetChunkByWorldPosition(blockWorldPos);
        if (ChunkDatas.TryGetValue(chunkPosV2, out ChunkData chunkData))
        {
            Vector3Int chunkOrigin = new Vector3Int(chunkPosV2.x, 0, chunkPosV2.y) * MeshBuilder.CHUNK_WIDTH;
            blockLocalPos = blockWorldPos - chunkOrigin;
            renderer = chunkData.ChunkRenderer;
            return true;
        }

        return false;
    }
    #endregion
}
