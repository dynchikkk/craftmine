using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Profiling;
using UnityEngine;

// test
public class TerrainGenerator : MonoBehaviour
{
    [SerializeField] private TerrainInfo _data;

    private FastNoiseLite[] _octaveNoises; 
    private FastNoiseLite _warpNoise;

    public void Init()
    {
        SetUpOctaves();
    }

    public BlockType[] GenerateTerrain(float xOffset, float zOffset)
    {
        //var height = MeshBuilder.CHUNK_HEIGHT;
        var width = MeshBuilder.CHUNK_WIDTH;
        var scale = MeshBuilder.BLOCK_SCALE;

        var terrain = new BlockType[width * MeshBuilder.CHUNK_HEIGHT * width];

        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < width; z++)
            {
                float worldX = x * scale + xOffset;
                float worldZ = z * scale + zOffset;

                float height = GetHeight(worldX, worldZ);
                float grassLayerHeight = 1f + _octaveNoises[0].GetNoise(worldX, worldZ) * 0.2f;
                float bedrockLayerHeight = 0.5f + _octaveNoises[0].GetNoise(worldX, worldZ) * 0.2f;

                for (int y = 0; y < height / scale; y++)
                {
                    int index = x + y * width * width + z * width;

                    // test
                    if (height - y * scale < grassLayerHeight)
                        terrain[index] = BlockType.Grass;
                    else if (y * scale < bedrockLayerHeight)
                        terrain[index] = BlockType.Wood;
                    else
                        terrain[index] = BlockType.Stone;
                }
            }
        }

        return terrain;
    }

    private void SetUpOctaves()
    {
        _octaveNoises = new FastNoiseLite[_data.OctaveSettings.Length];
        for (int i = 0; i < _octaveNoises.Length; i++)
        {
            _octaveNoises[i] = new();
            _octaveNoises[i].SetNoiseType(_data.OctaveSettings[i].NoiseType);
            _octaveNoises[i].SetFrequency(_data.OctaveSettings[i].Frequency);
        }

        _warpNoise = new();
        _warpNoise.SetNoiseType(_data.DomainWarp.NoiseType);
        _warpNoise.SetFrequency(_data.DomainWarp.Frequency);
        _warpNoise.SetDomainWarpAmp(_data.DomainWarp.Amplitude);
    }

    private float GetHeight(float x, float y)
    {
        _warpNoise.DomainWarp(ref x, ref y);

        float height = _data.BaseHeight;
        for (int i = 0; i < _octaveNoises.Length; i++)
        {
            float noise = _octaveNoises[i].GetNoise(x, y);
            height += noise * _data.OctaveSettings[i].Amplitude / 2;
        }

        return height;
    }

}


