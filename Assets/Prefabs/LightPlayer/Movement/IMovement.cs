using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    bool CanMove { get; }
    void Init();
    void SetMoveCondition(bool cond);
}
