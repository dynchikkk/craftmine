using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IJumpable
{
    event Action<Vector3> OnJump;
    void Jump();
}
